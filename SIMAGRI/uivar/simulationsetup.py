# -*- coding: utf-8 -*-
"""
Redesigned on Thu December 26 17:28:23 2016
@Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._configuration import *

class UIVarSimulationSetup():

  _Section_Name = 'SimulationSetup'

  # Simulation mode
  SimulationMode = None # CheckVar

  # Simulation horizon (crop growing season)
  Sim_StartYear = None # startyear2
  Sim_StartMonth = None #startmonth2
  Sim_EndYear = None # endyear2
  Sim_EndMonth = None # endmonth2

  # Prediction horizon (seasonal climate forecast)
  Pred_StartYear = None  # startyear3
  Pred_StartMonth = None  # startmonth3
  Pred_EndYear = None  # endyear3
  Pred_EndMonth = None  # endmonth3

  # Plating date (DOY)
  Planting_Date = None # planting_date

  def __init__(self):
    pass

  def initVars(self):
    if self.SimulationMode is not None:
      self.SimulationMode.set(0)


  def loadConfig(self):
    # Simulation mode
    get_config_value_into_tkinter_intvar(self._Section_Name, 'SimulationMode', self.SimulationMode)

    # Simulation horizon (crop growing season)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'Sim_StartYear', self.Sim_StartYear)
    get_config_value_into_pmw_combobox(self._Section_Name, 'Sim_StartMonth', self.Sim_StartMonth)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'Sim_EndYear', self.Sim_EndYear)
    get_config_value_into_pmw_combobox(self._Section_Name, 'Sim_EndMonth', self.Sim_EndMonth)

    # Prediction horizon (seasonal climate forecast)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'Pred_StartYear', self.Pred_StartYear)
    get_config_value_into_pmw_combobox(self._Section_Name, 'Pred_StartMonth', self.Pred_StartMonth)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'Pred_EndYear', self.Pred_EndYear)
    get_config_value_into_pmw_combobox(self._Section_Name, 'Pred_EndMonth', self.Pred_EndMonth)

    # Planting date (DOY)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'Planting_Date', self.Planting_Date)

  def saveConfig(self):
    # Simulation mode
    set_config_value_from_tkinter_intvar(self._Section_Name, 'SimulationMode', self.SimulationMode)

    # Simulation horizon (crop growing season)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'Sim_StartYear', self.Sim_StartYear)
    set_config_value_from_pmw_combobox(self._Section_Name, 'Sim_StartMonth', self.Sim_StartMonth)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'Sim_EndYear', self.Sim_EndYear)
    set_config_value_from_pmw_combobox(self._Section_Name, 'Sim_EndMonth', self.Sim_EndMonth)

    # Prediction horizon (seasonal climate forecast)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'Pred_StartYear', self.Pred_StartYear)
    set_config_value_from_pmw_combobox(self._Section_Name, 'Pred_StartMonth', self.Pred_StartMonth)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'Pred_EndYear', self.Pred_EndYear)
    set_config_value_from_pmw_combobox(self._Section_Name, 'Pred_EndMonth', self.Pred_EndMonth)

    # Planting date (DOY)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'Planting_Date', self.Planting_Date)

