# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
Created on Thu Dec 25 09:44:24 2016

@author: Seongkyu Lee, APEC Climate Center
"""

import os
import shutil

__author__ = "Seongkyu Lee"

def CopyModuleFiles(srcdir, destdir):
  w = os.walk(os.path.join(srcdir, "."))
  for dir_ in w:
    for file_ in dir_[2]:
      shutil.copy(os.path.join(dir_[0], file_), destdir)
