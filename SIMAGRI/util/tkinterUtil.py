# -*- coding: utf-8 -*-
"""
Created on Thu Dec 27 09:44:24 2016

@author: Seongkyu Lee, APEC Climate Center
"""

# import Tkinter
import tkinter #Tkinter is the PYthon interface to Tk, the GUI toolkit for Tcl/Tk

__author__ = "Seongkyu Lee"

# Center a window on the screen
# @ref: http://stackoverflow.com/questions/3352918/how-to-center-a-window-on-the-screen-in-tkinter
def Window_ScreenCenter(win):
  """
  centers a tkinter window
  :param win: the root or Toplevel window to center
  """
  win.update_idletasks()
  width = win.winfo_width()
  frm_width = win.winfo_rootx() - win.winfo_x()
  win_width = width + 2 * frm_width
  height = win.winfo_height()
  titlebar_height = win.winfo_rooty() - win.winfo_y()
  win_height = height + titlebar_height + frm_width
  x = win.winfo_screenwidth() // 2 - win_width // 2
  y = win.winfo_screenheight() // 2 - win_height // 2
  win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
  win.deiconify()
